//
//  test.swift
//  testxc
//
//  Created by fozu on 2023/11/21.
//

import UIKit
import Kingfisher

public class KingfisherTestingController: UIViewController {
    public override func viewDidLoad() {
        super.viewDidLoad()
        let imageView = UIImageView()
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(imageView)
        NSLayoutConstraint.activate([
            imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            imageView.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
        imageView.kf.setImage(with: URL(string: "https://www.imagebeauty.com/cdn/shop/files/checkout_logo_3_f48d009b-1fdf-47aa-8d41-19892be2e511.png?height=628&pad_color=ffffff&v=1649772125&width=1200"))
    }
}
