//
//  ViewController.swift
//  Example
//
//  Created by fozu on 2023/11/21.
//

import UIKit
import Kingfisher
import testxc

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.kf.setImage(with: URL(string: "https://www.imagebeauty.com/cdn/shop/files/checkout_logo_3_f48d009b-1fdf-47aa-8d41-19892be2e511.png?height=628&pad_color=ffffff&v=1649772125&width=1200"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(imageView)
        NSLayoutConstraint.activate([
            imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            imageView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            imageView.widthAnchor.constraint(equalToConstant: 200),
            imageView.heightAnchor.constraint(equalToConstant: 200),
        ])
                    
//        let controller = SDWebImageTestController()
        let controller = KingfisherTestingController()
        self.present(controller, animated: true)
    }
}

