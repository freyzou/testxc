Pod::Spec.new do |spec|
  spec.name         = "testxc"
  spec.version      = "1.0.8"
  spec.summary      = "test xcframework."
  spec.description  = <<-DESC
    A wrapper of alipaysdk.
                   DESC

  spec.homepage     = "https://gitlab.com/iosappkits/zyalipaykit"
  spec.license      = { :type => "MIT" }
  spec.author             = { "freyzou" => "747852289@qq.com" }
  
  spec.swift_version = '5.0'
  spec.platform     = :ios, "12.0"
  spec.source       = { :http => "https://gitlab.com/freyzou/public-resources/-/raw/main/testxc-v1.0.8.zip?ref_type=heads&inline=false"}
  
  spec.dependency 'Kingfisher', '7.10.0'
  spec.dependency 'SDWebImage', '5.18.3'
  
  spec.vendored_frameworks = 'testxc.xcframework'

end
